# GraphQL PWA and Page API

This API helps to build PWA and Pages tables. 

To be able to use it through GraphQL Playground,

1. Install Prisma
```
$ sudo npm -g install prisma
```

2. Install GraphQL
```
$  sudo npm install -g graphql-cli
```

3. Build and run the container in pwa-generator-module/api
```
$ docker-compose build
$ docker-compose up
```

4. Deploy service using prisma. Be sure the container is still up.
```
$ prisma deploy --force
```

5. Acess API playgound at 
```
localhost:8080/playground
```