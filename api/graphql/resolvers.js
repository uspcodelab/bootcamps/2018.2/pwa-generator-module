const fetch = require("node-fetch");

const resolvers = {
  Query: {
    pwa: (_, args, context, info) => {
      return context.prisma.query.pWA(
        {
          where: {
            hackathon_id: args.hackathon_id
          }
        },
        info
      );
    },
    page: (_, args, context, info) => {
      return context.prisma.query.page(
        {
          where: {
            edition_id: args.edition_id
          }
        },
        info
      );
    },
  },
  Mutation: {
    createPWA: (_, args, context, info) => {
      return context.prisma.mutation.createPWA(
        {
          data: {
            slug: args.slug,
            hackathon_id: args.hackathon_id
          }
        },
        info
      );
    },
    createPage: (_, args, context, info) => {
      return context.prisma.mutation.createPage(
        {
          data: {
            pallete_primary: args.pallete_primary,
            pallete_secondary: args.pallete_secondary,
            logo: args.logo,
            edition_id : args.edition_id,
            path: args.path,
            pwa: {
              connect: {
                id: args.pwa_id
              }
            }
          }
        },
        info
      );
    },
    updatePWA: (_, args, context, info) => {
      return context.prisma.mutation.updatePWA(
        {
          data: {
            slug: args.slug,
          },
          where: {
            id: args.pwa_id
          }
        },
        info
      );
    },
    updatePage: (_, args, context, info) => {
      return context.prisma.mutation.updatePage(
        {
          data: {
            pallete_primary: args.pallete_primary,
            pallete_secondary: args.pallete_secondary,
            logo: args.logo,
            path: args.path,
            // pwa: { 
            //   connect: {
            //     id: args.pwa
            //   }
            // }
          },
          where: {
            id: args.page_id
          }
        },
        info
      );
    },
  }
};

export default resolvers;
