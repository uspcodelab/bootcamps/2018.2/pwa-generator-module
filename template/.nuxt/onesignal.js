window.$OneSignal = window.OneSignal = window.OneSignal || [];

OneSignal.push(['init', {
  "appId": "8d601dcc-08e4-4b42-bb4e-d6535d218a80",
  "allowLocalHostAsSecureOrigin": true,
  "welcomeNotification": {
    "disable": false
  },
  "allowLocalhostAsSecureOrigin": true
}]);

export default function (ctx, inject) {
  inject('OneSignal', OneSignal)
}
