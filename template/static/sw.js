importScripts('/_nuxt/workbox.3de3418b.js')

const workboxSW = new self.WorkboxSW({
  "cacheId": "HackathonUSP",
  "clientsClaim": true,
  "directoryIndex": "/"
})

workboxSW.precache([
  {
    "url": "/_nuxt/492131f658c6c3c1b6cd.js",
    "revision": "2d3c7c2087085bf8f534ef5f400de891"
  },
  {
    "url": "/_nuxt/a0cbcce188fe3f3f8cac.js",
    "revision": "36c80f9dc0cffb49c59fd4cfa17601ea"
  },
  {
    "url": "/_nuxt/a2f9bf96b6225fd1bc40.js",
    "revision": "587d4cdb5e7ffd23305e0e2c630151e7"
  },
  {
    "url": "/_nuxt/a4cb5858b4391d16037d.js",
    "revision": "531309aaf615c8b52f344d04402b030e"
  },
  {
    "url": "/_nuxt/a8db3da3492244186e48.js",
    "revision": "7ec81ecce5b56cc87b2ac3153589b1df"
  },
  {
    "url": "/_nuxt/ons.40ac840e.js",
    "revision": "bdfc64563fadaafd6b988984058fefd9"
  }
])


workboxSW.router.registerRoute(new RegExp('/_nuxt/.*'), workboxSW.strategies.cacheFirst({}), 'GET')

workboxSW.router.registerRoute(new RegExp('/.*'), workboxSW.strategies.networkFirst({}), 'GET')

